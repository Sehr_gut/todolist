package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import main.model.Point;
import main.model.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/toDoList")
public class PointController {




  @Autowired
  private ToDoRepository toDoRepository;

  @GetMapping("/")
  public List<Point> list(){
    Iterable<Point> pointIterable = toDoRepository.findAll();
    ArrayList<Point> toDoList = new ArrayList<>();
    for(Point point : pointIterable) {
      toDoList.add(point);
    }
    return toDoList;
  }

  @PostMapping("/")
  public int add(Point point){
    Point newPoint = toDoRepository.save(point);
    return newPoint.getId();
  }


  @GetMapping("/search")
  public List<Point> get(@RequestParam(required = false) String query){

    List<Point> toDoList = toDoRepository.findByNameContaining(query, Sort.by(Sort.Direction.ASC, "date"));
    return toDoList;

  }

  @GetMapping("/{id}")
  public ResponseEntity get(@PathVariable int id){

    Optional<Point> optionalPoint = toDoRepository.findById(id);
    if(optionalPoint.isEmpty()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }
    return new ResponseEntity(optionalPoint.get(), HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> remove(@PathVariable int id){

    Optional<Point> optionalPoint = toDoRepository.findById(id);
    if (optionalPoint.isPresent()){
      toDoRepository.deleteById(id);
      return ResponseEntity.ok().build();
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }
  }

  @DeleteMapping("/")
  public ResponseEntity<Void> removeAll(){

    Iterable<Point> pointIterable = toDoRepository.findAll();
      for(Point point : pointIterable) {
        toDoRepository.delete(point);
      }
      return ResponseEntity.ok().build();
  }

  @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
  public ResponseEntity<Void> change(Point point, @PathVariable int id) {
    Optional<Point> optionalPoint = toDoRepository.findById(id);
    if (optionalPoint.isPresent()) {
      Iterable<Point> pointIterable = toDoRepository.findAll();
      ArrayList<Point> toDoList = new ArrayList<>();
      for (Point p : pointIterable) {
        if (p.getId() != id) {
          toDoList.add(p);
        } else {
          toDoList.add(point);
        }
      }
      toDoRepository.saveAll(toDoList);
      return ResponseEntity.ok().build();
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }
  }
}
