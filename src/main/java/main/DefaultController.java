package main;

import java.util.ArrayList;
import main.model.Point;
import main.model.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultController
{
  @Autowired
  private ToDoRepository toDoRepository;

  @Value("${someParameter.value}")
  private Integer someParameter;

  @RequestMapping("/")
  public String index(Model model)
  {
    Iterable<Point> pointIterable = toDoRepository.findAll();
    ArrayList<Point> point = new ArrayList<>();
    for(Point book : pointIterable) {
      point.add(book);
    }
    model.addAttribute("toDoList", point);
    model.addAttribute("pointCount", point.size());
    model.addAttribute("someParameter", someParameter);
    return "index";
  }
}
