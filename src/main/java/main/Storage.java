package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import main.model.Point;


public class Storage {

  private static int pointCount = 1;
  private static HashMap<Integer, Point> toDoMap = new HashMap<>();

  public static int addPoint(Point point){
    toDoMap.put(pointCount, point);
    point.setId(pointCount);
    return pointCount++;
  }

  public static List<Point> getToDoList(){
    ArrayList<Point> toDoList = new ArrayList<>();
    toDoList.addAll(toDoMap.values());
    return toDoList;
  }

  public static Point getPoint(int pointId){
    if (toDoMap.containsKey(pointId)){
      return toDoMap.get(pointId);
    } else {
      return null;
    }
  }

  public static Map<Integer, Point> removePoint(int pointId){
    if(toDoMap.containsKey(pointId)){
      toDoMap.remove(pointId);
    }else {
      return null;
    }
    return toDoMap;
  }

  public static Map<Integer, Point> removeAll(){
    toDoMap.clear();
    return toDoMap;
  }

  public static Map<Integer, Point> updatePoint(int pointId, Point point){
    if (toDoMap.containsKey(pointId)){
      toDoMap.remove(pointId);
      toDoMap.put(pointId, point);
    }
    return toDoMap;


  }

}
