$(function(){

    const appendPoint = function(data){
        var pointCode = '<a href="#" class="point-link" data-id="' +
            data.id + '">' + data.name + '</a><br>';
        $('#point-list')
            .append('<div>' + pointCode + '</div>');
    };

    //Loading books on load page
//    $.get('/books/', function(response)
//    {
//        for(i in response) {
//            appendBook(response[i]);
//        }
//    });

    //Show adding book form
    $('#show-add-point-form').click(function(){
        $('#point-form').css('display', 'flex');
    });

    //Closing adding book form
    $('#point-form').click(function(event){
        if(event.target === this) {
            $(this).css('display', 'none');
        }
    });

    //Getting book
    $(document).on('click', '.point-link', function(){
        var link = $(this);
        var pointId = link.data('id');
        $.ajax({
            method: "GET",
            url: '/toDoList/' + pointId,
            success: function(response)
            {
                var code = '<span>Дэдлайн: ' + response.date + '</span>';
                link.parent().append(code);
            },
            error: function(response)
            {
                if(response.status == 404) {
                    alert('Дело не найдено');
                }
            }
        });
        return false;
    });

    //Adding book
    $('#save-point').click(function()
    {
        var data = $('#point-form form').serialize();
        $.ajax({
            method: "POST",
            url: '/toDoList/',
            data: data,
            success: function(response)
            {
                $('#point-form').css('display', 'none');
                var point = {};
                point.id = response;
                var dataArray = $('#point-form form').serializeArray();
                for(i in dataArray) {
                    point[dataArray[i]['name']] = dataArray[i]['value'];
                }
                appendPoint(point);
            }
        });
        return false;
    });

});